


import 'package:barg_test/domain/model/User.dart';

abstract class MainRepository{
  Future<List<User>> getUsers();

}