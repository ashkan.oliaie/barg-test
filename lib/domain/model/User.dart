

import 'package:freezed_annotation/freezed_annotation.dart';

import 'Friend.dart';


part 'User.freezed.dart';
part 'User.g.dart';

@freezed
 class User with _$User {
  const factory User({
    required String guid,
    required bool isOwner,
    required String balance,
    required String picture,
    required int age,
    required String eyeColor,
    required String name,
    required String gender,
    required String company,
    required String email,
    required String phone,
    required String address,
    required String about,
    required String registered,
    required double latitude,
    required double longitude,
    required List<String> tags,
    required String greeting,
    required String favoriteFruit,
    required List<Friend> friends,
  })=_User;


  factory User.fromJson(Map<String, dynamic> json) =>  _$UserFromJson(json);

  // factory User.fromJson(Map<String,dynamic> json)=>_$UserFromJson(json);
}