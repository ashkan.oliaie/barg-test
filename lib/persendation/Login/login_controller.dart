




import 'package:barg_test/Utils/Const/Validators.dart';
import 'package:barg_test/data/states/GlobalState.dart';
import 'package:barg_test/domain/model/User.dart';

import 'package:barg_test/persendation/Router/screens.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginController extends GetxController{


  GlobalKey<FormState> form = GlobalKey<FormState>();
  GlobalController globalController=Get.find();

  var emailController=TextEditingController();
  // var passController=TextEditingController();

  // ignore: prefer_function_declarations_over_variables
  var emailValidator= (value){return basicValidator(value,"Email");};

  // // ignore: prefer_function_declarations_over_variables
  // var passValidator= (value){return basicValidator(value,"Password");};


  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }


  onFormSubmit(){

    bool? validated=form.currentState?.validate();

    if(validated==true){

     login();
    }
    // print(userController.text);
    // print(passController.text);
  }


  login(){

    String email=emailController.text;



    User? user=globalController.users.firstWhereOrNull((mappedUser){
      return (mappedUser.email== email );
    });



    if(user!=null){
      globalController.setMyProfile(user);

      Get.toNamed(HOME_ROUTE,arguments:user);
    }else{
      Get.showSnackbar(
          const GetSnackBar( title:"Error",message: "User not found.",backgroundColor:Colors.redAccent,duration: Duration(milliseconds: 1500))
      );
    }
  }

  //
  //
  // var userController=TextEditingController().obs;
  // var passwordController=TextEditingController().obs;
  //
  // var counter=0.obs;







}