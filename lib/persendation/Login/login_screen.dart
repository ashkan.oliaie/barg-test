



import 'package:barg_test/data/datasource/main_repository_impl.dart';
import 'package:barg_test/data/states/GlobalState.dart';
import 'package:barg_test/persendation/Login/login_controller.dart';
import 'package:barg_test/persendation/Splash/splash_controller.dart';
import 'package:barg_test/persendation/Widgets/CustomTextField.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class LoginScreen extends GetWidget<LoginController>{




  @override
  Widget build(BuildContext context) {





    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 20,vertical: 30),
          child: Column(
            children: [

             Form(
                 key: controller.form,
                 child: Column(
                   children: [
                     CustomTextField(hint:"Email",validator: controller.emailValidator, controller: controller.emailController),
                     // CustomTextField(hint:"Password",validator: controller.passValidator, controller: controller.passController)

                   ],
                 )),


              ElevatedButton(onPressed: (){
                controller.onFormSubmit();

              }, child: Text("Submit"))
            ],
          )
        ),
      )
    );
  }

}