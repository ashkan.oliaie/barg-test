




import 'package:barg_test/domain/model/User.dart';
import 'package:barg_test/persendation/Profile/profile_controller.dart';
import 'package:barg_test/persendation/Router/screens.dart';
import 'package:barg_test/persendation/Widgets/ImageHolder.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';

class ProfileScreen extends GetWidget<ProfileController> {
  @override
  Widget build(BuildContext context) {


    User user=Get.arguments;
    User? me=controller.me;
    bool isMe=user.guid==me?.guid;



    return Scaffold(

      appBar: AppBar(
        leadingWidth: 25,
        title: Row(
          children: [
            GestureDetector(
              onTap: (){},
              child: Container(
                  padding: EdgeInsets.all(10),
                  child: ImageHolder(imageUrl: user.picture, size: 40,)),
            ),

            Text(user.name + " Profile", style: TextTheme().subtitle1,),
          ],
        ),
      ),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column(

            children: [
              Container(
                padding: const EdgeInsets.all(10),
                decoration:  BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color:Colors.white,
                  boxShadow:  [
                    BoxShadow(
                        color: Colors.deepPurpleAccent.withOpacity(0.2),
                        blurRadius: 5,
                        spreadRadius: 4
                    )
                  ],

                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text('Balance : \$${user.balance}'),
                    const SizedBox(height: 10,),
                    Row(
                      children: [
                        Text('Age : ${user.age}'),

                      ],
                    ),
                    const SizedBox(height: 10,),
                    Text('Registered : ${user.registered}'),
                    const SizedBox(height: 10,),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text('About : '),
                        Container(
                            width: 300,
                            child: Text('${user.about}',overflow: TextOverflow.ellipsis,maxLines: 2,)),


                      ],
                    ),
                    SizedBox(height: 10,),
                    if(isMe)ElevatedButton(onPressed: (){}, child: Container(child:Text("Edit")))
                  ],
                ),
              ),
              const SizedBox(height: 20,),

              if(!user.friends.isEmpty)Container(
                padding: const EdgeInsets.all(10),
                decoration:  BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color:Colors.white,
                  boxShadow:  [
                    BoxShadow(
                        color: Colors.deepPurpleAccent.withOpacity(0.2),
                        blurRadius: 5,
                        spreadRadius: 4
                    )
                  ],

                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    const Text("Friends : ",style: TextStyle(fontSize: 16),),
                    const SizedBox(height: 10,),
                    Container(
                      height: 30,

                      child: ListView(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        children: (user.friends as List).map((friend) => GestureDetector(
                            child: Container(
                              margin: const EdgeInsets.symmetric(horizontal: 10),
                              child: ElevatedButton(

                                style: ButtonStyle(
                                  visualDensity: VisualDensity.compact,
                                  backgroundColor: MaterialStateProperty.all(Colors.blueAccent)
                                ),
                                child: Text(friend.name),
                                onPressed: (){
                                  controller.onFriendClick(friend);
                                },
                              ),
                            ),
                        )).toList(),
                      ),
                    )
                  ],
                ),
              ),
              if(!user.friends.isEmpty)const SizedBox(height: 20,),
              Container(

                width: double.infinity,
                padding: const EdgeInsets.all(10),
                decoration:  BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color:Colors.white,
                  boxShadow:  [
                    BoxShadow(
                        color: Colors.deepPurpleAccent.withOpacity(0.2),
                        blurRadius: 5,
                        spreadRadius: 4
                    )
                  ],

                ),

                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Greeting : ",style: TextStyle(fontSize: 17),),
                    Container(

                      child:Text(user.greeting) ,
                    )
                  ],
                ),
              )

            ],
          ),
        ),
      ),

    );
  }

}