

import 'package:barg_test/data/states/GlobalState.dart';
import 'package:barg_test/domain/model/Friend.dart';
import 'package:barg_test/domain/model/User.dart';
import 'package:barg_test/persendation/Router/screens.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';

class ProfileController extends GetxController {


  GlobalController globalController=Get.find();


  User? me;



  @override
  void onInit() {
    me=globalController.me;

    super.onInit();
  }

  void onFriendClick(Friend friend) {

    User? friendProfile = globalController.users.firstWhereOrNull((element) => element.guid==friend.guid);

    // ignore: unnecessary_null_comparison
    if(friendProfile!=null){
      Get.toNamed(PROFILE_ROUTE,arguments: friendProfile,preventDuplicates: false);
    }else{
      Get.showSnackbar(
          const GetSnackBar( title:"Error",message: "User not found.",backgroundColor:Colors.redAccent,duration: Duration(milliseconds: 1500),)
      );
    }




  }




}