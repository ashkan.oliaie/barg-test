

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {

  var validator;
  TextEditingController controller;
  String hint;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: TextFormField(
        validator: validator,
        controller: controller,
        decoration:  InputDecoration(
          // suffixIcon: secure ? ,
            focusColor: Colors.deepPurpleAccent,
            fillColor: Colors.grey.shade200,
            filled: true,
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.deepPurpleAccent,width:1.5),
              borderRadius: BorderRadius.circular(15.0),
            ),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent),
                borderRadius: BorderRadius.circular(15.0)),
            border: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent),
                borderRadius: BorderRadius.circular(15.0)),
            hintText:hint,

            contentPadding: EdgeInsets.all(15.0),
            floatingLabelBehavior:
            FloatingLabelBehavior.always),
      ),
    );
  }

  CustomTextField({
    required this.validator,
    required this.controller,
    required this.hint,
  });
}
