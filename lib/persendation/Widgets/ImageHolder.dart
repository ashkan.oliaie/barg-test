

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ImageHolder extends StatelessWidget {
  String imageUrl;
  double size;
  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(50),
      child: Image.network(imageUrl,
        width: size,height:size,
       fit: BoxFit.contain,
       errorBuilder: (ctx,_,__){
        return Container(
          color:Colors.grey.shade400,
            width: size,height:size,
            child: const Icon(Icons.error));
      },),
    );
  }

  ImageHolder({
    required this.imageUrl,
     this.size=40,
  });
}
