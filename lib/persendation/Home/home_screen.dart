import 'package:barg_test/data/datasource/main_repository_impl.dart';
import 'package:barg_test/data/states/GlobalState.dart';
import 'package:barg_test/persendation/Home/home_controller.dart';
import 'package:barg_test/persendation/Home/widgets/SkeletonCard.dart';
import 'package:barg_test/persendation/Home/widgets/UserCard.dart';
import 'package:barg_test/persendation/Login/login_controller.dart';
import 'package:barg_test/persendation/Router/screens.dart';
import 'package:barg_test/persendation/Splash/splash_controller.dart';
import 'package:barg_test/persendation/Widgets/CustomTextField.dart';
import 'package:barg_test/persendation/Widgets/ImageHolder.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class HomeScreen extends GetWidget<HomeController> {


  @override
  Widget build(BuildContext context) {
    // List users = controller.users.value;

    // print(users);





    return Scaffold(
        appBar: AppBar(
          leadingWidth: 25,
          title: Row(
            children: [
              GestureDetector(
                onTap: (){
                 controller.onProfileClick();
                },
                child: Container(
                    padding: EdgeInsets.all(10),
                    child: ImageHolder(imageUrl: controller.me.picture, size: 40,)),
              ),

              Text(controller.me.name, style: TextTheme().subtitle1,),
            ],
          ),
        ),
        body: SafeArea(
          child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
              child: GetBuilder<HomeController>(
                assignId: true,
                builder: (logic) {

                  List users=logic.users;

                  return ListView(

                    children: users.isEmpty ?
                    List.generate(10,(index)=>SkeletonCard()).toList()
                    :
                    [...users.map((user) => UserCard(user:user,onPress:(){
                      controller.onSelect(user);
                    },)),],
                  );
                },
              )
          ),
        )
    );
  }

}