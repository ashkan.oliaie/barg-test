


import 'package:barg_test/data/states/GlobalState.dart';
import 'package:barg_test/domain/model/User.dart';

import 'package:barg_test/persendation/Router/screens.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';

class HomeController extends GetxController {

  GlobalController globalController=Get.find<GlobalController>();


  List users=[].obs;
  User me=Get.arguments;

  @override
  void onReady() {

    super.onReady();
  }

  @override
  void onInit() {
    getUsers();
    super.onInit();
  }


  void getUsers()async{

    await Future.delayed(const Duration(milliseconds: 1500));
    print("waited");

    users.assignAll(globalController.users);
    update();




  }

  void onSelect(User user) async{

    Get.toNamed(PROFILE_ROUTE,arguments:user);
  }

  void onProfileClick() {
    Get.to(PROFILE_ROUTE,arguments:me);
  }


}