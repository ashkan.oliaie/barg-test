

import 'package:barg_test/persendation/Widgets/ImageHolder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:skeleton_loader/skeleton_loader.dart';

class SkeletonCard extends StatelessWidget {





  @override
  Widget build(BuildContext context) {



    return SkeletonLoader(

      builder: Container(
        margin: const EdgeInsets.symmetric(vertical: 10,horizontal: 10),
        padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 10),
        decoration: BoxDecoration(
            boxShadow: const [
              BoxShadow(
                  color: Colors.deepPurpleAccent,
                  blurRadius: 2,
                  spreadRadius: 1
              )
            ],
            color: Colors.white,
            borderRadius: BorderRadius.circular(20)
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                ImageHolder(imageUrl: ""),
                const SizedBox(width: 15,),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // Container(height:10,width: 100,color:Colors.grey,margin: EdgeInsets.symmetric(vertical: 10),),
                    // Container(height:10,width: 150,color:Colors.grey),

                  ],
                )
              ],
            ),
            const SizedBox(height: 15,),
            // Container(height:10,width: double.infinity,color:Colors.grey),
          ],
        ),
      ),
    );
  }
}
