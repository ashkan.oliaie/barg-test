


import 'package:barg_test/persendation/Widgets/ImageHolder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UserCard extends StatelessWidget {

  var user;
  Function onPress;


  @override
  Widget build(BuildContext context) {




    return GestureDetector(
      onTap: (){
        onPress();
      },
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 10,horizontal: 10),
        padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 10),
        decoration: BoxDecoration(
          boxShadow:  [
            BoxShadow(
              color: Colors.deepPurpleAccent.withOpacity(0.2),
              blurRadius: 5,
              spreadRadius: 4
            )
          ],
          color: Colors.white,
          borderRadius: BorderRadius.circular(20)
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                ImageHolder(imageUrl: user.picture),
                const SizedBox(width: 15,),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(user.name),
                    Text(user.email),
                  ],
                )
              ],
            ),
            const SizedBox(height: 15,),
            Text('Address : ${user.address}')
          ],
        ),
      ),
    );
  }

  UserCard({
    required this.user,
    required this.onPress,
  });
}
