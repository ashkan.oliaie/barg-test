




import 'package:barg_test/persendation/Home/home_binding.dart';
import 'package:barg_test/persendation/Home/home_screen.dart';
import 'package:barg_test/persendation/Login/login_binding.dart';
import 'package:barg_test/persendation/Login/login_screen.dart';
import 'package:barg_test/persendation/Profile/profile_binding.dart';
import 'package:barg_test/persendation/Profile/profile_screen.dart';
import 'package:barg_test/persendation/Router/screens.dart';
import 'package:barg_test/persendation/Splash/splash_binding.dart';
import 'package:barg_test/persendation/Splash/splash_screen.dart';
import 'package:get/get.dart';

final routers=[
    GetPage(name: LOGIN_ROUTE, page:()=>LoginScreen(),binding:LoginBinding() ),
    GetPage(name: SPLASH_ROUTE, page:()=>SplashScreen(),binding:SplashBinding() ),
    GetPage(name: HOME_ROUTE, page:()=>HomeScreen(),binding:HomeBinding() ),
    GetPage(name: PROFILE_ROUTE, page:()=>ProfileScreen(),binding:ProfileBinding() ),
];