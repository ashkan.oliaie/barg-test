

import 'package:barg_test/persendation/Splash/splash_controller.dart';
import 'package:get/get.dart';

class SplashBinding extends Bindings{
  
  @override
  void dependencies() {
   Get.lazyPut(() => SplashController(mainRepository: Get.find()));
  }
  
}