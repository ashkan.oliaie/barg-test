

import 'package:barg_test/data/datasource/main_repository_impl.dart';
import 'package:barg_test/data/states/GlobalState.dart';
import 'package:barg_test/domain/model/User.dart';

import 'package:barg_test/domain/repository/main_repository.dart';
import 'package:barg_test/persendation/Router/screens.dart';
import 'package:get/get.dart';

class SplashController extends GetxController{
  final MainRepositoryImpl mainRepository;

  final GlobalController globalController=Get.find();

   SplashController({
    required this.mainRepository,
  });





   @override
  void onReady() {
    // TODO: implement onReady
     getUsers();
    super.onReady();
  }


  void getUsers()async{
     List<User> users=await mainRepository.getUsers();


     globalController.addUsers(users);
     Get.toNamed(LOGIN_ROUTE);
  }


}