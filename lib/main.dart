
import 'package:barg_test/main_bindings.dart';
import 'package:barg_test/persendation/Router/router.dart';
import 'package:barg_test/persendation/Router/screens.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';



void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      initialRoute: SPLASH_ROUTE,
      getPages: routers,
      initialBinding: MainBinding(),

    );
  }
}

