

import 'package:barg_test/data/datasource/main_repository_impl.dart';
import 'package:barg_test/data/states/GlobalState.dart';
import 'package:barg_test/domain/repository/main_repository.dart';
import 'package:get/get.dart';

class MainBinding extends Bindings{
  @override
  void dependencies() {
    Get.lazyPut<MainRepositoryImpl>(() => MainRepositoryImpl());
    Get.lazyPut<GlobalController>(() => GlobalController());
  }
  
}