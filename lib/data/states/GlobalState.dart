



import 'package:barg_test/domain/model/User.dart';
import 'package:get/get.dart';

class GlobalController extends GetxController{

  List users=[];
  User? me;

  addUsers( List<User> passedUsers){
    users=[...passedUsers];
  }

  setMyProfile(User user){
    me=user;
  }

}