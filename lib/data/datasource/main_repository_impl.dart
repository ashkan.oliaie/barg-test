

import 'dart:convert';


import 'package:barg_test/Domain/repository/main_repository.dart';
import 'package:barg_test/domain/model/User.dart';


import 'package:flutter/services.dart' show rootBundle;

Future<String> getJson() {
  return rootBundle.loadString('assets/jsons/users.json');
}

class MainRepositoryImpl extends MainRepository{

  @override
  Future<List<User>> getUsers() async{
    await Future.delayed(const Duration(milliseconds: 1000));
    // ignore: non_constant_identifier_names
    String StringedJson=await getJson();

    var jsoned=json.decode(StringedJson);


    return (jsoned as List).map((e) => User.fromJson(e)).toList();
  }

}